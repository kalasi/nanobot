// ISC License (ISC)
//
// Copyright (c) 2016, Austin Hellyer <hello@austinhellyer.me>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
// REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
// AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
// INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
// LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
// OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
// PERFORMANCE OF THIS SOFTWARE.

pub use error::{Error, Result};
pub use postgres::types::FromSql;
pub use postgres::rows::Rows;
pub use ::bot::config::*;
pub use ::ext::commands::Context;
pub use ::utils::{get_location, get_server};

use postgres::types::ToSql;
use postgres::{Connection as PgConnection, Result as PgResult};
use std::sync::MutexGuard;

pub type Params<'a> = Vec<&'a ToSql>;
pub type PgConn<'a> = MutexGuard<'a, PgConnection>;
pub type PgRes<'a> = PgResult<Rows<'a>>;
